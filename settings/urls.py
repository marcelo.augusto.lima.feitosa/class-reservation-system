from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from settings import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('reservation/v1/', include('reservation.urls'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
