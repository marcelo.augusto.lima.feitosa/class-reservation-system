# Sistema de Reservas Uespi

Sistema de reservas de salas e equipamentos da UESPI.

## Iniciando

Para rodar o projeto os seguintes requerimentos devem ser satisfeitos:
- docker
- docker-compose
- python 3.9

O repositório pode ser clonado usando:
``
git clone https://gitlab.com/antonioroddev/sistema-de-reservas-uespi.git
``

Após clonar o respositório, recomenda-se o uso de um ambiente virtual do python, que pode ser criado usando:
 ``
python -m venv nome_da_env 
``

O sistema pode ser visto ao usar no terminal o comando``docker-compose up``.
Em seguida o site pode ser visto acessando [Localhost:8000](localhost:8000)
