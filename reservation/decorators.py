from django.shortcuts import redirect
from django.contrib import messages


def unauthenticated_user(view_func):
    def wrapper_func(request, *args, **kwargs):
        if request.request.user.is_authenticated:
            return redirect('homepage')
        else:
            return view_func(request, *args, **kwargs)

    return wrapper_func


def allowed_users(allowed_roles=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            if "all" in allowed_roles:
                return view_func(request, *args, **kwargs)

            if request.request.user.is_authenticated:
                if request.request.user.role:
                    group = request.request.user.role
                else:
                    group = "common"
            else:
                group = "common"

            if group in allowed_roles:
                return view_func(request, *args, **kwargs)

            else:
                messages.warning(request.request, "You're not allowed")
                return redirect('homepage')

        return wrapper_func
    return decorator