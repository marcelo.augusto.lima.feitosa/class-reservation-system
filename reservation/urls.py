from django.urls import path

from reservation import views

urlpatterns = [
    # homepage
    path('', views.HomePage.as_view(), name='homepage'),

    # classroom paths
    path('classes/', views.ClassroomList.as_view(), name='classroom-list'),
    path('classes/create/', views.CreateClassroom.as_view(), name='classroom-create'),
    path('classes/<int:pk>/', views.ClassroomDetail.as_view(), name='classroom-detail'),
    path('classes/<int:id>/event/', views.EventClassroomList.as_view(), name='classroom-events'),
    path('classes/<int:id>/event/<int:pk>/', views.EventDetail.as_view(), name='classroom-event'),
    path('classes/<int:id>/event/<int:pk>/update/', views.UpdateEvent.as_view(), name='classroom-event-update'),
    path('classes/<int:id>/event/<int:pk>/delete/', views.DeleteEvent.as_view(), name='classroom-event-delete'),
    path('classes/<int:id>/event/<int:pk>/equipment/', views.AddEquipment.as_view(), name='event-equipment'),
    path('classes/<int:pk>/create/', views.CreateEvent.as_view(), name='classroom-event-create'),
    path('classes/<int:pk>/update/', views.UpdateClassroom.as_view(), name='classroom-update'),
    path('classes/<int:pk>/delete/', views.DeleteClassroom.as_view(), name='classroom-delete'),

    # equipment paths
    path('equipments/', views.EquipmentListView.as_view(), name='equipment-list'),
    path('equipments/create/', views.CreateEquipment.as_view(), name='equipment-create'),
    path('equipments/<int:pk>/', views.EquipmentDetailView.as_view(), name='equipment-detail'),
    path('equipments/<int:pk>/update/', views.UpdateEquipment.as_view(), name='equipment-update'),
    path('equipments/<int:pk>/delete/', views.DeleteEquipment.as_view(), name='equipment-delete'),

    # campus paths
    path('campus/', views.CampusList.as_view(), name='campus-list'),
    path('campus/create/', views.CreateCampus.as_view(), name='campus-create'),
    path('campus/<int:pk>/', views.CampusDetail.as_view(), name='campus-detail'),
    path('campus/<int:pk>/update/', views.UpdateCampus.as_view(), name='campus-update'),
    path('campus/<int:pk>/delete/', views.DeleteCampus.as_view(), name='campus-delete'),

    # event paths
    path('event/', views.UnauthorizedEvents.as_view(), name='event-list'),
    path('event/<int:pk>/', views.UnauthorizedEventDetail.as_view(), name='event-detail'),
    path('event/<int:pk>/update/', views.UpdateUnauthorizedEvent.as_view(), name='event-update'),
    path('event/<int:pk>/delete/', views.DeleteUnauthorizedEvent.as_view(), name='event-delete'),
    path('event/<int:pk>/authorize/', views.Authorize.as_view(), name='authorize'),

    # pre-schedule and reservate
    path('classes/<int:pk>/preschedule/', views.PreSchedule.as_view(), name='pre-schedule'),
    path('classes/<int:pk>/reservate/', views.AddMultipleEvents.as_view(), name='reservate'),

    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout')
]
