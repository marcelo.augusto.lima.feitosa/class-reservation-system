from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView, TemplateView
from django.urls import reverse_lazy, reverse

from settings import settings
from .forms import *

from reservation.models import *
from reservation.validations import *
from reservation.decorators import unauthenticated_user, allowed_users


# Equipments Classes
class EquipmentListView(ListView):
    model = Equipment
    template_name = 'reservation/equipment/equipment_list.html'

    @allowed_users(["all"])
    def dispatch(self, request, *args, **kwargs):
        return super(EquipmentListView, self).dispatch(request, *args, **kwargs)


class EquipmentDetailView(DetailView):
    model = Equipment
    template_name = 'reservation/equipment/equipment_detail.html'

    @allowed_users(["all"])
    def dispatch(self, request, *args, **kwargs):
        return super(EquipmentDetailView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EquipmentDetailView, self).get_context_data(**kwargs)

        ev_eq = EventEquipment.objects.filter(equipment=self.object)
        event_list = [reservation.event for reservation in ev_eq]

        names = [event.name for event in event_list]
        dates = [event.date.strftime("%d/%m/%Y") for event in event_list]
        times = [event.time.strftime("%H:%M") for event in event_list]

        context["Events"] = zip(names, dates, times)

        return context


class CreateEquipment(CreateView):
    model = Equipment
    template_name = 'reservation/equipment/equipment_create.html'
    form_class = CreateEquipmentForm

    @allowed_users(["director"])
    def dispatch(self, request, *args, **kwargs):
        return super(CreateEquipment, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        class_id = self.object.pk
        return reverse_lazy('equipment-detail', kwargs={'pk': class_id})


class AddEquipment(CreateView):
    model = EventEquipment
    template_name = "reservation/event/equipment_form.html"
    form_class = AddEquipmentForm

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.event = Event.objects.get(pk=self.kwargs["pk"])

        if not validate_equipment_occupation(obj.event.date, obj.event.time, obj.equipment):
            messages.error(self.request, "Aparelho em uso")
            return super(AddEquipment, self).form_invalid(form)

        obj.save()
        return super(AddEquipment, self).form_valid(form)

    @allowed_users(["teacher", "director"])
    def dispatch(self, request, *args, **kwargs):
        return super(CreateEquipment, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('classroom-event', kwargs={"id": self.kwargs["id"], "pk": self.kwargs["pk"]})


class UpdateEquipment(UpdateView):
    model = Equipment
    template_name = 'reservation/equipment/equipment_update.html'
    form_class = UpdateEquipmentForm

    @allowed_users(["director"])
    def dispatch(self, request, *args, **kwargs):
        return super(UpdateEquipment, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('equipment-detail', kwargs={"pk": self.object.pk})


class DeleteEquipment(DeleteView):
    model = Equipment
    template_name = "reservation/create_form.html"
    success_url = reverse_lazy('equipment-list')

    @allowed_users(["director"])
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteEquipment, self).dispatch(request, *args, **kwargs)


# Classroom Classes
class ClassroomList(ListView):
    model = ClassRoom
    template_name = 'reservation/classroom/classroom_list.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ClassroomList, self).get_context_data(**kwargs)
        context["object_list"] = sorted(context["object_list"], key=lambda x: x.institution.name)
        return context


class ClassroomDetail(DetailView):
    model = ClassRoom
    template_name = 'reservation/classroom/classroom_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ClassroomDetail, self).get_context_data(**kwargs)

        names = [event.name for event in Event.objects.filter(classroom=self.object)]
        dates = [event.date.strftime("%d/%m/%Y") for event in Event.objects.filter(classroom=self.object)]
        times = [event.time.strftime("%H:%M") for event in Event.objects.filter(classroom=self.object)]

        context["Events"] = zip(names, dates, times)

        return context

    @allowed_users(["all"])
    def dispatch(self, request, *args, **kwargs):
        return super(ClassroomDetail, self).dispatch(request, *args, **kwargs)


class CreateClassroom(CreateView):
    model = ClassRoom
    template_name = 'reservation/classroom/classroom_create.html'
    form_class = ClassesForm
    success_url = reverse_lazy('classroom-list')

    @allowed_users(["prefecture"])
    def dispatch(self, request, *args, **kwargs):
        return super(CreateClassroom, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        print(form.cleaned_data.get('teste'))
        return super(CreateClassroom, self).form_valid(form)

    def get_success_url(self):
        class_id = self.object.pk
        return reverse_lazy('classroom-detail', kwargs={'pk': class_id})


class UpdateClassroom(UpdateView):
    model = ClassRoom
    template_name = 'reservation/classroom/classroom_create.html'
    form_class = UpdateClassesForm

    @allowed_users(["prefecture"])
    def dispatch(self, request, *args, **kwargs):
        return super(UpdateClassroom, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('classroom-detail', kwargs={"pk": self.object.pk})


class DeleteClassroom(DeleteView):
    model = ClassRoom
    success_url = reverse_lazy('classroom-list')

    @allowed_users(["prefecture"])
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteClassroom, self).dispatch(request, *args, **kwargs)


# Event Classes
class UnauthorizedEvents(ListView):
    model = Event
    template_name = 'reservation/event/unauthorized_event_list.html'

    @allowed_users(["teacher", "prefecture"])
    def dispatch(self, request, *args, **kwargs):
        return super(UnauthorizedEvents, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(UnauthorizedEvents, self).get_context_data(**kwargs)
        user = self.request.user

        events = [event for event in Event.objects.filter(status="Aguardando confirmação")]

        if user.is_authenticated and user.role != "prefecture":
            events = [event for event in Event.objects.filter(status="Aguardando confirmação",
                                                              responsible=user)]

        dates = [event.date.strftime("%d/%m/%Y") for event in events]
        times = [event.time.strftime("%H:%M") for event in events]

        context["Events"] = zip(events, dates, times)
        return context


class Authorize(UpdateView):
    model = Event
    template_name = "reservation/event/event_authorize.html"
    fields = []

    @allowed_users(["prefecture"])
    def dispatch(self, request, *args, **kwargs):
        return super(Authorize, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        self.object.status = "Confirmado"
        return super(Authorize, self).form_valid(form)

    def get_success_url(self):
        class_id = self.object.classroom.id
        event_id = self.object.id
        return reverse_lazy('classroom-event', kwargs={"pk": event_id, "id": class_id})


class EventClassroomList(ListView):
    model = Event
    template_name = 'reservation/event/event_list.html'

    @allowed_users(["all"])
    def dispatch(self, request, *args, **kwargs):
        return super(EventClassroomList, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(EventClassroomList, self).get_context_data(**kwargs)
        context['classroom'] = ClassRoom.objects.get(pk=self.kwargs["id"])

        events = [event for event in Event.objects.filter(classroom=self.kwargs["id"], status="Confirmado")]
        dates = [event.date.strftime("%d/%m/%Y")
                 for event in Event.objects.filter(classroom=self.kwargs["id"], status="Confirmado")]
        times = [event.time.strftime("%H:%M")
                 for event in Event.objects.filter(classroom=self.kwargs["id"], status="Confirmado")]

        context["Events"] = zip(events, dates, times)

        return context


class EventDetail(DetailView):
    model = Event
    template_name = 'reservation/event/event_detail.html'

    @allowed_users(["all"])
    def dispatch(self, request, *args, **kwargs):
        return super(EventDetail, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EventDetail, self).get_context_data(**kwargs)

        obj_list = EventEquipment.objects.filter(event=self.object)
        context['equipments'] = [obj.equipment for obj in obj_list]
        context['event_date'] = self.object.date.strftime("%d/%m/%Y")
        context['event_time'] = self.object.time.strftime("%H:%M")

        return context


class UnauthorizedEventDetail(DetailView):
    model = Event
    template_name = 'reservation/event/unauthorized_event_detail.html'

    @allowed_users(["teacher", "prefecture"])
    def dispatch(self, request, *args, **kwargs):
        return super(UnauthorizedEventDetail, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UnauthorizedEventDetail, self).get_context_data(**kwargs)

        obj_list = EventEquipment.objects.filter(event=self.object)
        context['equipments'] = [obj.equipment for obj in obj_list]
        context['event_date'] = self.object.date.strftime("%d/%m/%Y")
        context['event_time'] = self.object.time.strftime("%H:%M")

        return context


class CreateEvent(CreateView):
    model = Event
    template_name = 'reservation/event/event_create.html'
    form_class = CreateEventForm

    @allowed_users(["dean"])
    def dispatch(self, request, *args, **kwargs):
        return super(CreateEvent, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        class_id = self.object.classroom.pk
        return reverse_lazy('classroom-events', kwargs={'id': class_id})

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.classroom = ClassRoom.objects.get(pk=self.kwargs["pk"])

        if not self.request.user.is_authenticated:
            messages.error(self.request, "Usuário não autenticado")
            return super(CreateEvent, self).form_invalid(form)

        obj.responsible = self.request.user

        if not validate_occupation(obj.date, obj.time, obj.duration, obj.classroom):
            messages.error(self.request, "Data e hora inválidos")
            return super(CreateEvent, self).form_invalid(form)

        obj.save()
        return super(CreateEvent, self).form_valid(form)


class PreSchedule(CreateView):
    model = Event
    template_name = 'reservation/event/event_create.html'
    form_class = PreScheduleForm

    @allowed_users(["teacher"])
    def dispatch(self, request, *args, **kwargs):
        return super(PreSchedule, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        class_id = self.object.pk
        return reverse_lazy('event-detail', kwargs={'pk': class_id})

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.classroom = ClassRoom.objects.get(pk=self.kwargs["pk"])

        if not self.request.user.is_authenticated:
            messages.error(self.request, "Usuário não autenticado")
            return super(PreSchedule, self).form_invalid(form)

        obj.responsible = self.request.user
        obj.status = "Aguardando confirmação"

        if not validate_occupation(obj.date, obj.time, obj.duration, obj.classroom):
            messages.error(self.request, "Data e hora inválidos")
            return super(PreSchedule, self).form_invalid(form)

        obj.save()
        return super(PreSchedule, self).form_valid(form)


class AddMultipleEvents(CreateView):
    model = Event
    template_name = 'reservation/event/event_reservate.html'
    form_class = AddMultipleEventsForm
    success_url = reverse_lazy('event-list')

    @allowed_users(["director"])
    def dispatch(self, request, *args, **kwargs):
        return super(AddMultipleEvents, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        initial_date = form.cleaned_data.get('date')
        final_date = form.cleaned_data.get('limit_date')

        time = form.cleaned_data.get('time')
        duration = form.cleaned_data.get('duration')

        classroom = ClassRoom.objects.get(pk=self.kwargs["pk"])

        date_list = []
        while initial_date <= final_date:
            date_list.append(initial_date)
            initial_date += timedelta(days=7)

        for date in date_list:
            if not validate_occupation(date, time, duration, classroom):
                messages.error(self.request, "Data e hora inválidos")
                return super(AddMultipleEvents, self).form_invalid(form)

        if not self.request.user.is_authenticated:
            messages.error(self.request, "Usuário não autenticado")
            return super(AddMultipleEvents, self).form_invalid(form)

        responsible = self.request.user

        for date in date_list:
            obj = form.save(commit=False)
            obj.pk = None
            obj.classroom = classroom
            obj.responsible = responsible
            obj.date = date

            obj.save()

        return super(AddMultipleEvents, self).form_valid(form)

    def get_success_url(self):
        class_id = self.kwargs['pk']
        return reverse_lazy('classroom-events', kwargs={"id": class_id})


class UpdateEvent(UpdateView):
    model = Event
    template_name = 'reservation/event/event_update.html'
    form_class = UpdateEventsForm

    @allowed_users(["dean", "director"])
    def dispatch(self, request, *args, **kwargs):
        return super(UpdateEvent, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UpdateEvent, self).get_context_data(**kwargs)
        context["date"] = self.object.date.strftime("%Y-%m-%d")
        context["time"] = self.object.time.strftime("%H:%M:%S")
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)

        if not self.request.user == self.object.responsible:
            messages.error(self.request, "Usuário inválido")
            return super(UpdateEvent, self).form_invalid(form)

        if not validate_update_occupation(obj.date, obj.time, obj.duration, obj.classroom, self.object):
            messages.error(self.request, "Data e hora inválidos")
            return super(UpdateEvent, self).form_invalid(form)

        obj.save()
        return super(UpdateEvent, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('classroom-event', kwargs={"pk": self.object.pk, "id": self.kwargs["id"]})


class UpdateUnauthorizedEvent(UpdateView):
    model = Event
    template_name = 'reservation/event/event_update.html'
    form_class = UpdateEventsForm

    @allowed_users(["dean"])
    def dispatch(self, request, *args, **kwargs):
        return super(UpdateUnauthorizedEvent, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UpdateUnauthorizedEvent, self).get_context_data(**kwargs)
        context["date"] = self.object.date.strftime("%Y-%m-%d")
        context["time"] = self.object.time.strftime("%H:%M:%S")
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)

        if not self.request.user == self.object.responsible:
            messages.error(self.request, "Usuário inválido")
            return super(UpdateUnauthorizedEvent, self).form_invalid(form)

        if not validate_update_occupation(obj.date, obj.time, obj.duration, obj.classroom, self.object):
            messages.error(self.request, "Data e hora inválidos")
            return super(UpdateUnauthorizedEvent, self).form_invalid(form)

        obj.save()
        return super(UpdateUnauthorizedEvent, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('event-detail', kwargs={"pk": self.object.pk})


class DeleteEvent(DeleteView):
    model = Event
    success_url = reverse_lazy('event-list')

    @allowed_users(["dean", "director"])
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteEvent, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        class_id = self.kwargs["id"]
        return reverse_lazy('classroom-events', kwargs={"id": class_id})


class DeleteUnauthorizedEvent(DeleteView):
    model = Event
    success_url = reverse_lazy('event-list')

    @allowed_users(["prefecture"])
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteUnauthorizedEvent, self).dispatch(request, *args, **kwargs)


# Campus Classes
class CampusList(ListView):
    model = Campus
    template_name = 'reservation/campus/campus_list.html'

    @allowed_users(["all"])
    def dispatch(self, request, *args, **kwargs):
        return super(CampusList, self).dispatch(request, *args, **kwargs)


class CampusDetail(DetailView):
    model = Campus
    template_name = 'reservation/campus/campus_detail.html'

    @allowed_users(["all"])
    def dispatch(self, request, *args, **kwargs):
        return super(CampusDetail, self).dispatch(request, *args, **kwargs)


class CreateCampus(CreateView):
    model = Campus
    template_name = 'reservation/campus/campus_create.html'
    form_class = CreateCampusForm
    success_url = reverse_lazy('campus-list')

    @allowed_users(["prefecture"])
    def dispatch(self, request, *args, **kwargs):
        return super(CreateCampus, self).dispatch(request, *args, **kwargs)


class UpdateCampus(UpdateView):
    model = Campus
    template_name = 'reservation/campus/campus_update.html'
    form_class = UpdateCampusForm
    success_url = reverse_lazy('campus-list')

    @allowed_users(["prefecture"])
    def dispatch(self, request, *args, **kwargs):
        return super(UpdateCampus, self).dispatch(request, *args, **kwargs)


class DeleteCampus(DeleteView):
    model = Campus
    success_url = reverse_lazy('campus-list')

    @allowed_users(["prefecture"])
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteCampus, self).dispatch(request, *args, **kwargs)


# Login Classes
class LoginView(View):
    @unauthenticated_user
    def get(self, request):
        data = {'form': LoginForm()}
        return render(request, 'reservation/login.html', data)

    @unauthenticated_user
    def post(self, request):
        form = LoginForm(data=request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            if username and password \
                    and authenticate(username=username, password=password):
                login(request, authenticate(username=username, password=password))
                return HttpResponseRedirect(reverse('login'))

        data = {
            'form': form,
            'error': 'Usuário ou senha inválidos'
        }
        return render(request, 'reservation/login.html', data)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse('login'))


class HomePage(TemplateView):
    template_name = "reservation/index.html"
