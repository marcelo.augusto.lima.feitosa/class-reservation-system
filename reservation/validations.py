from datetime import timedelta

from reservation.models import *


def validate_user(request):
    if request.user.is_authenticated:
        return True


def validate_occupation(date, time, duration, classroom):
    allowed_hours = [8, 10, 12, 14, 16, 18, 20]
    if time.hour not in allowed_hours or time.minute != 0:
        return False

    elif time.hour + duration > 22:
        return False

    for event in Event.objects.filter(date=date, classroom=classroom):
        if event.time.hour == time.hour:
            return False

        elif event.time.hour < time.hour < event.duration + event.time.hour:
            return False

    return True


def validate_update_occupation(date, time, duration, classroom, act_event):
    allowed_hours = [8, 10, 12, 14, 16, 18, 20]
    if time.hour not in allowed_hours or time.minute != 0:
        return False

    elif time.hour + duration > 22:
        return False

    for event in Event.objects.filter(date=date, classroom=classroom):
        if event != act_event:
            if event.time.hour == time.hour:
                return False

            elif event.time.hour < time.hour < event.duration + event.time.hour:
                return False

    return True


def validate_equipment_occupation(date, time, equipment):
    events_query = EventEquipment.objects.filter(equipment=equipment)
    events = [obj.event for obj in events_query]

    for event in events:
        if event.date == date:
            if event.time.hour == time.hour:
                return False
            elif event.time.hour < time.hour < event.time.hour + event.duration:
                return False

    return True