from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms

from .models import *


class CustomUsuarioCreateForm(UserCreationForm):

    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name', 'role')
        labels = {'username': 'Username/E-mail', 'role': 'role'}

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data["username"]
        if commit:
            user.save()
        return user


class CustomUsuarioChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name', 'role')


class LoginForm(forms.Form):
    username = forms.CharField(label='Usuário')
    password = forms.CharField(label='Senha', widget=forms.PasswordInput)


class CreateEquipmentForm(forms.ModelForm):
    class Meta:
        model = Equipment
        fields = ['name', 'brand', 'serial_number']


class AddEquipmentForm(forms.ModelForm):
    class Meta:
        model = EventEquipment
        fields = ['equipment']


class UpdateEquipmentForm(forms.ModelForm):
    class Meta:
        model = Equipment
        fields = ['name']


class ClassesForm(forms.ModelForm):
    class Meta:
        model = ClassRoom
        fields = ['capacity', 'institution']


class UpdateClassesForm(forms.ModelForm):
    class Meta:
        model = ClassRoom
        fields = ['capacity', 'institution']


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'date', 'time', 'duration', 'classroom']


class CreateEventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'date', 'time', 'duration']


class PreScheduleForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'date', 'time', 'duration']


class AddMultipleEventsForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'date', 'time', 'duration']

    limit_date = forms.DateField()


class UpdateEventsForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['date', 'time', 'duration']


class CreateCampusForm(forms.ModelForm):
    class Meta:
        model = Campus
        fields = ['name']


class UpdateCampusForm(forms.ModelForm):
    class Meta:
        model = Campus
        fields = ['name']
