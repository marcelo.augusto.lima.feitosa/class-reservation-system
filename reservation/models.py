from abc import ABC

from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError("O email é obrigatório")

        email = self.normalize_email(email)

        user = self.model(email=email, username=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_superuser', True) is not True:
            raise ValueError('Superuser precisa ser superuser')
        if extra_fields.get('is_staff', True) is not True:
            raise ValueError('Superuser precisa ser staff')

        return self._create_user(email, password, **extra_fields)


class CustomUser(AbstractUser):
    possible_choices = [
        ("common", "common"),
        ("dean", "dean"),
        ("prefecture", "prefecture"),
        ("director", "director"),
        ("teacher", "teacher")
    ]
    email = models.EmailField('Email', unique=True)
    role = models.CharField(max_length=255, null=False, default="common", choices=possible_choices, blank=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'role']

    def __str__(self):
        return self.email

    objects = UserManager()


class Campus(models.Model):
    class Meta:
        default_permissions = ()

    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class ClassRoom(models.Model):
    class Meta:
        default_permissions = ()

    capacity = models.IntegerField(default=30)
    institution = models.ForeignKey(Campus, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pk)


class Equipment(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False)
    brand = models.CharField(max_length=200, null=False, blank=False)
    serial_number = models.IntegerField(null=False, blank=False, unique=True)

    def __str__(self):
        return self.name


class Event(models.Model):
    duration = [
        (2, "2 horas"),
        (4, "4 horas"),
        (6, "6 horas"),
        (8, "8 horas"),
        (10, "10 horas"),
        (12, "12 horas")
    ]

    possible_states = [
        ("Confirmado", "Confirmado"),
        ("Aguardando confirmação", "Aguardando confirmação"),
    ]

    name = models.CharField(max_length=255, null=False, blank=False)
    date = models.DateField(blank=False, null=False)
    time = models.TimeField(blank=False, null=False)
    duration = models.IntegerField(choices=duration, blank=False, null=False)
    classroom = models.ForeignKey(ClassRoom, on_delete=models.CASCADE)
    responsible = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    status = models.CharField(max_length=255, null=False, default="Confirmado", choices=possible_states, blank=False)


class EventEquipment(models.Model):
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE, blank=False, null=False)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, blank=False, null=False)