(() => {
  const elements = document.querySelectorAll('.navigation__icons')

  elements.forEach(e => {
    e.addEventListener('mouseover', event => {
      console.log(event.currentTarget.lastElementChild.style.display = "block")
    })

    e.addEventListener('mouseout', event => {
      console.log(event.currentTarget.lastElementChild.style.display = "none")
    })
  })
})();
