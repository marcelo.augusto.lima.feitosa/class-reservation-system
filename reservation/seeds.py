import random
from reservation.models import *

CAMPUS_LIST = [
    "Torquato",
    "Pirajá",
    "Clovis Moura",
]

CLASSES_LIST = [random.randint(20, 40) for count in range(20)]

EQUIPMENTS = [
    ("Computer", "Samsung", 1234),
    ("Cellphone", "Nokia", 23789),
    ("Projector", "Xiaomi", 98762),
    ("Computer", "Acer", 969069),
]
USER_LIST = [
    ("prefecture@gmail.com", "prefecture", "Leonardo", "Coelho", "senha123"),
    ("common@gmail.com", "common", "Marcelo", "Feitosa", "senha123"),
    ("teacher@gmail.com", "teacher", "Antonio", "Rodrigues", "senha123"),
    ("dean@gmail.com", "dean", "Thiago", "Carvalho", "senha123"),
    ("director@gmail.com", "director", "Antonio", "Rodrigues", "senha123"),
]


def populate_user_db():
    for user in USER_LIST:
        CustomUser.objects.create_user(user[0],
                                       user[4],
                                       first_name=user[2],
                                       last_name=user[3],
                                       role=user[1])


def populate_equipments_db():
    for equipment in EQUIPMENTS:
        Equipment.objects.create(name=equipment[0],
                                 brand=equipment[1],
                                 serial_number=equipment[2],
                                 )


def populate_campus_db():
    for campus in CAMPUS_LIST:
        Campus.objects.create(name=campus)


def populate_classes_db():
    for classes in CLASSES_LIST:
        ClassRoom.objects.create(capacity=classes,
                                 institution=Campus.objects.all()[random.randint(0, len(Campus.objects.all()) - 1)])


def populate_db():
    populate_user_db()
    populate_equipments_db()
    populate_campus_db()
    populate_classes_db()